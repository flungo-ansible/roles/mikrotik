# MikroTik Ansible Role

This role provides modules for use with MikroTik RouterOS and SwitchOS devices as well as useful tasks which provide abstraction for various features. This role is a work in progress and does not support management of all possible configurations.

The default functionality of the role makes the provided modules available for use within your playbook. [Additional tasks](#tasks) are provided which provide abstractions and help automate the configuration of your MikroTik devices.

This role has been designed for use with Ansible 2.3 and above and is not compatible with older versions.

## Usage

The role should be included into your project's role path as you would with any other Ansible role. You can then use the role in a playbook as with this example:

```yaml
- hosts: mikrotik
  gather_facts: no
  roles:
    - mikrotik
  tasks:
    - name: Get the time from the client
      ros_command:
        command: /system date get time
        put: True
```

In this example, the `mikrotik` role is loaded and then a task is run using this role which gets the time from the device. Note that `gather_facts` should be disabled when working with MikroTik hosts.

## Connection

The modules provided by this role use an SSH connection to the MikroTik device in order to run commands. This should be configured as you would with any other host using your SSH config and inventory files to specify the details required for connection. To setup password-less authentication, see [this tutorial on setting up RouterOS with RSA keys](http://jcutrer.com/howto/networking/mikrotik/routeros-ssh-publickeyauth-rsa-keys).

## Tasks

Utility tasks which can be used in your playbooks include:

- `configure.yml` - configure a MikroTik network ([docs](docs/tasks/configure.md))
- `configure-debug.yml` - get the effective configuration used for each host
- `update-objects.yml` - find and update objects on a MikroTik device

To use one of these tasks in a playbook, you can use the [include_role](https://docs.ansible.com/ansible/latest/include_role_module.html) with the `tasks_from` parameter. Documentation for each task is provided in the comments at the head of the relevant task file or under `docs/tasks` in a file matching the name of the task.
