# Copyright (c) 2017 Fabrizio Lungo <opensource@flungo.me>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# this is a virtual module that is entirely implemented server side

ANSIBLE_METADATA = {'metadata_version': '1.0',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: ros_command
short_description: Executes a command on a MikroTik RouterOS device
description:
    - Executes a command on a MikroTik device using an SSH connection. This is equivalent to the M(raw) module but for use with MikroTik devices.
options:
    command:
        description:
            - The command to run on the MikroTik device.
        required: true
    properties:
        description:
            - A dictionary of properties which will be appended to the end of the given command in the format C(key="value").
        type: dict
    ignore_empty:
        description:
            - A list of properties to ignore if they have an empty value. These properties will not be added to the end of the command.
        type: list
    put:
        description:
            - Whether or not the command should be wrapped in a C(:put) command so that the output of the command is printed.
        type: bool
    where:
        description:
            - Whether or not the properties should be given as a where clause.
            - As a where clause the C(where) keyword will be added to the command before the properties are listed and empty properties will be matched with C(!property_name).
        type: bool
notes:
    - The command given is wrapped in an error catching block so that failures can be reliably identified, but this does mean that meaningful error messages are supressed.
    - You will need to disable disable fact gathering using C(gather_facts: no) on plays which run on MikroTik devices as they do not support this.
    - The C(environment) keyword does not work with ros_command.
author:
    - Fabrizio Lungo (@flungo)
'''

EXAMPLES = '''
- name: Get the time from the MikroTik router
  ros_command:
    command: /system clock get time
    put: True
'''

RETURN = '''
changed:
    description: Whether or not the run has changed the state of the host
    returned: always
    type: boolean
    sample: True
stdout:
    description: The command standard output
    returned: always
    type: string
    sample: 'ar9344\\r\\n'
stderr:
    description: The command standard error
    returned: always
    type: string
    sample: ''
rc:
    description: The command return code (0 means success)
    returned: always
    type: int
    sample: 0
stdout_lines:
    description: The command standard output split in lines
    returned: always
    type: list
    sample: [u'ar9344']
'''
