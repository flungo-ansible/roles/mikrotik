# MikroTik Ansible Role: Configure Task

A task called `configure.yml` is included as part of the role which is able to apply configurations based on a declarative specification provided through the variables. These variables can be set on the role itself or globally in your ansible project in the group_vars or host_vars. See [configuration](#configuration) for detail about the variables for providing configuration.

The following is an example playbook using this task:

```yaml
- hosts: mikrotik
  gather_facts: no
  tasks:
    - include_role:
        name: mikrotik
        tasks_from: configure
```

It is recommended that you create a group for your MikroTik devices or for each network and use group variables for global configurations which apply across your network and individual host variables to configure device specifics such as interface configurations.

## Supported features

The following sub-menus can be configured for MikroTik devices running RouterOS or SwitchOS:

- `/interface bridge` - see [interface_bridge](#interface_bridge)
- `/interface bridge port` - see [interface_bridge](#interface_bridge)
- `/interface bridge vlan` - see [interface_bridge](#interface_bridge)
- `/interface ethernet` - see [interface_ethernet](#interface_ethernet)
- `/interface list` - see [interface_list](#interface_list)
- `/interface list member` - see [interface_list](#interface_list)
- `/interface vlan` - see [interface_vlan](#interface_vlan)
- `/ip address` - see [ip_address](#ip_address)
- `/ip dhcp-client` - see [ip_dhcp-client](#ip_dhcp-client)
- `/ip dhcp-server` - see [ip_dhcp-server](#ip_dhcp-server)
- `/ip dhcp-server lease` - see [ip_dhcp-server](#ip_dhcp-server)
- `/ip dhcp-server network` - see [ip_dhcp-server_network](#ip_dhcp-server_network)
- `/ip pool` - see [ip_pool](#ip_pool)
- `/system clock` - see [system_clock](#system_clock)
- `/system identity` - see [system_identity](#system_identity)

The following sub-menu can be configured on [Cloud Router Switch models](https://wiki.mikrotik.com/wiki/Manual:CRS_features#Cloud_Router_Switch_models):

- `/interface ethernet switch` - see [interface_ethernet_switch](#interface_ethernet_switch)
- `/interface ethernet switch egress-vlan-tag` - see [interface_ethernet_switch_egress-vlan-tag](#interface_ethernet_switch_egress-vlan-tag)
- `/interface ethernet switch egress-vlan-translation` - see [interface_ethernet_switch_egress-vlan-translation](#interface_ethernet_switch_egress-vlan-translation-interface_ethernet_switch_ingress-vlan-translation)
- `/interface ethernet switch ingress-vlan-translation` - see [interface_ethernet_switch_ingress-vlan-translation](#interface_ethernet_switch_egress-vlan-translation-interface_ethernet_switch_ingress-vlan-translation)
- `/interface ethernet switch mac-based-vlan` - see [interface_ethernet_switch_mac-based-vlan](#interface_ethernet_switch_mac-based-vlan)
- `/interface ethernet switch port` - see [interface_ethernet_switch_port](#interface_ethernet_switch_port)
- `/interface ethernet switch trunk` - see [interface_ethernet_switch_trunk](#interface_ethernet_switch_trunk)
- `/interface ethernet switch vlan` - see [interface_ethernet_switch_vlan](#interface_ethernet_switch_vlan)

## Configuration

The configuration of the network should be provided through variables. By default there are 3 customisable variables which are merged together: `mikrotik_group`, `mikrotik_host`, `mikrotik`. The precedence of these variables is in that order such that configurations from `mikrotik_group` have the lowest precedence and `mikrotik` has the highest. `mikrotik_group` should be used in group variable files; `mikrotik_host` should be used in host variable files; and `mikrotik` can be be used at the task level.

If for some reason you do not want the default merging behaviour to take place then `mikrotik_effective` can be overridden but this is not recommended unless you understand what you are doing.

When merging these variables, sections with lists of objects will be unioned such that all elements will be included in the effective configuration with those of lower precedence being inlcuded first. This means that if there are conflicting properties defined for the same matched object, it will be temporarily configured with the lower precedence property before being set to the higher precedence property.

In most cases, the keys in the configuration hash correspond to the MikroTik sub-menu which the configuration section with spaces replaced by `_`. For example, configurations in the `ip_address` section create/update objects in the `/ip address` sub-menu. There are some exceptions where objects of a sub-menu are contained within the objects of its parent menu as well as where additional sections are provided for convinience. See [sections](#sections) for documentation relating to each supported section as well as details about the order in which tasks are applied.

### MikroTik Objects

In general, sections which map to MikroTik sub-menus are given as a list of objects where the object contains the required parameters for that object, a `properties` field for any optional properties and in some cases fields which contain a list of nested objects. These nested objects are created based on the parent object which they are defined under - for example, the ports that are attached to a bridge interface.

For objects defined in the configuration there is no validation done except for ensuring that the required fields are specified and the value of fields is validated on the router. By using lists of objects, it is possible to ensure that the objects are created in dependency order. The order of tasks contained in the role cannot be changed and so if the order of tasks conflicts with your dependencies, it may be required to split your configuration and invoke the `configure.yml` task multiple times.

When an object is updated, a subset of the required fields (specified in the documentation) are used to match an object on the router. These fields are chosen so that they uniquely identify the object and so will either be a unique field or a unique tuple. If no object is found matching these fields, then an object will be created.

The property names used will always match exactly the names of the properties as they are defined on a MikroTik device.

If two items in the list of objects match the same object then the updates will be applied in the order they are listed but could have undesired effects if there are conflicting properties between the two matches.

### MikroTik settings

Some sub-menus do not contain objects and just contain settings. In these sections the configuration simply provides a map of property names to values which will be set in the respective sub-menu.

### Abstract Sections

Additional sections, beyond those mapping to MikroTik sub-menus, will be added in future versions allowing for more abstracted configuration of MikroTik devices.

## Tags

Tasks are tagged based the configuration hierarchy which it uses and in turn the command path on the MikroTik device. For example, the task that creates Ethernet interfaces using commands under `/interface ethernet` and using the `mikrotik.interface.ethernet` configuration is tagged with `mikrotik`, `mikrotik.interface` and `mikrotik.interface.ethernet`. This allows for you to restrict which components will be configured when running a playbook.

## Sections

This section outlines the configurations which can be set in this variable. Examples will be given as defined under the `mikrotik` variable but can also be configured under `mikrotik_group` or `mikrotik_host`  (see [configuration](#configuration)).

- [interface_ethernet](#interface_ethernet)
- [interface_vlan](#interface_vlan)
- [interface_bridge](#interface_bridge)
- [interface_ethernet_switch](#interface_ethernet_switch)
- [interface_ethernet_switch_trunk](#interface_ethernet_switch_trunk)
- [interface_ethernet_switch_egress-vlan-tag](#interface_ethernet_switch_egress-vlan-tag)
- [interface_ethernet_switch_egress-vlan-translation](#interface_ethernet_switch_egress-vlan-translation-interface_ethernet_switch_ingress-vlan-translation)
- [interface_ethernet_switch_ingress-vlan-translation](#interface_ethernet_switch_egress-vlan-translation-interface_ethernet_switch_ingress-vlan-translation)
- [interface_ethernet_switch_port](#interface_ethernet_switch_port)
- [interface_ethernet_switch_vlan](#interface_ethernet_switch_vlan)
- [ip_address](#ip_address)
- [ip_pool](#ip_pool)
- [ip_dhcp-client](#ip_dhcp-client)
- [ip_dhcp-server](#ip_dhcp-server)
- [ip_dhcp-server_network](#ip_dhcp-server_network)
- [system_clock](#system_clock)
- [system_identity](#system_identity)

The order in which sections are described here, are the order in which they are applied to the MikroTik device. This is based on the order which the [MikroTik export command](https://wiki.mikrotik.com/wiki/Manual:Configuration_Management#Exporting_Configuration) orders with minor alterations, attempting to ensure that the order maintains any dependency order.

### interface_ethernet

Ethernet interfaces are matched based on their `default-name`. See [Manual:Interface/Ethernet](https://wiki.mikrotik.com/wiki/Manual:Interface/Ethernet) for all available properties.

If the name of any interface is changed, it should be the defined name which is used elsewhere in the configuration file. Where an ethernet interface is referenced by another (such as in the `mater-port` properties) you should ensure that the interface being referenced is defined before the one referencing it.

```yaml
mikrotik:
  interface_ethernet:
    - default-name: {{ default-name }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
    ...
```

The following configuration is an example where `ether1` is configured as an independent port and `ether[2-5]` are setup in switch mode using `ether2` as the master:

```yaml
mikrotik:
  interface_ethernet:
    - default-name: ether1
      properties:
        name: ether1-wan
    - default-name: ether2
      properties:
        name: ether2-master
    - default-name: ether3
      properties:
        name: ether3-slave
      master-port: ether2-master
    - default-name: ether4
      properties:
        name: ether4-slave
        master-port: ether2-master
    - default-name: ether5
      properties:
        name: ether5-slave
        master-port: ether2-master
```

### interface_vlan

VLAN interfaces are matched using the `interface` and `vlan-id`. All other properties, including the `name` are held within the `properties` hash - see [Manual:Interface/VLAN](https://wiki.mikrotik.com/wiki/Manual:Interface/VLAN) for all available properties.

```yaml
mikrotik:
  interface_vlan:
    - interface: {{ interface }}
      vlan-id: {{ vlan-id }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
    ...
```

The following example creates the vlan interfaces required for the [Port based VLAN tagging #2 (Trunk and Hybrid ports)](https://wiki.mikrotik.com/wiki/Manual:Interface/VLAN#Port_based_VLAN_tagging_.232_.28Trunk_and_Hybrid_ports.29) setup example.

```yaml
mikrotik:
  interface_vlan:
    - interface: ether2
      vlan-id: 200
      properties:
        name: eth2-vlan200
    - interface: ether2
      vlan-id: 300
      properties:
        name: eth2-vlan300
    - interface: ether2
      vlan-id: 400
      properties:
        name: eth2-vlan400
    - interface: ether6
      vlan-id: 300
      properties:
        name: eth6-vlan300
    - interface: ether6
      vlan-id: 400
      properties:
        name: eth6-vlan400
    - interface: ether7
      vlan-id: 200
      properties:
        name: eth7-vlan200
    - interface: ether7
      vlan-id: 400
      properties:
        name: eth7-vlan400
    - interface: ether8
      vlan-id: 200
      properties:
        name: eth8-vlan200
    - interface: ether8
      vlan-id: 300
      properties:
        name: eth8-vlan300
```

### interface_bridge

Bridges are matched unsing their `name`. See [Manual:Interface/Bridge](https://wiki.mikrotik.com/wiki/Manual:Interface/Bridge) for all available properties.

For each bridge, ports can be configured which will be added to that bridge. Ports are matched based on the bridge they are defined under and the `interface` which they attach to the bridge.

Bridges also allow for VLANs to be configured. These are matched using the `vlan-ids` which the configuration represents.

```yaml
mikrotik:
  interface_bridge:
    - name: {{ name }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
      port:
        - interface: {{ interface_name }}
          properties:
            {{ property_name }}: {{ property_value }}
            ...
        ...
      vlan:
        - vlan-ids: {{ vlan-ids }}
          properties:
            {{ property_name }}: {{ property_value }}
            ...
        ...
    ...
```

The following example creates a bridge for the local area network configured with VLANs which has a trunk port (`ether2`) and 3 hybrid ports (`ether3`, `ether4`, `ether5`) as per the [VLAN Example #2 (Trunk and Hybrid Ports)](https://wiki.mikrotik.com/wiki/Manual:Interface/Bridge#VLAN_Example_.232_.28Trunk_and_Hybrid_Ports.29). The example also configured a basic guest network using a separate bridge with `ether6` and `ether7` on its own bridge.

```yaml
mikrotik:
  interface_bridge:
    - name: lan
      properties:
        comment: Local Area Network
        vlan-filtering: yes
      port:
        - interface: ether2
        - interface: ether3
          properties:
            pvid: 300
        - interface: ether4
          properties:
            pvid: 400
        - interface: ether5
          properties:
            pvid: 500
      vlan:
        - vlan-ids: 300
          properties:
            tagged: ether2,ether4,ether5
            untagged: ether3
        - vlan-ids: 400
          properties:
            tagged: ether2,ether3,ether5
            untagged: ether4
        - vlan-ids: 500
          properties:
            tagged: ether2,ether3,ether4
            untagged: ether5
    - name: guest
      properties:
        comment: Guest Network
      port:
        - interface: ether6
        - interface: ether7
```

### interface_ethernet_switch

The configuration of the Ethernet switch can be configured with the `interface_ethernet_switch` section on [supported devices](https://wiki.mikrotik.com/wiki/Manual:Switch_Chip_Features#Introduction). See [Manual:CRS features#Global Settings](https://wiki.mikrotik.com/wiki/Manual:CRS_features#Global_Settings) for all available properties.

```yaml
mikrotik:
  interface_ethernet_switch:
    {{ property_name }}: {{ property_value }}
    ...
```

The following example sets ether5 port as a mirror0 analyzer port for both ingress and egress mirroring as per the [port mirroring example](https://wiki.mikrotik.com/wiki/Manual:CRS_examples#Port_Based_Mirroring):

```yaml
mikrotik:
  interface_ethernet_switch:
    ingress-mirror0: ether5
    egress-mirror0: ether5
```

### interface_ethernet_switch_trunk

Ethernet switch trunks are matched using their `name` and requires `member-ports`. See [Manual:CRS features#Trunking](https://wiki.mikrotik.com/wiki/Manual:CRS_features#Trunking) for all available properties.

```yaml
mikrotik:
  interface_ethernet_switch_trunk:
    - name: {{ name }}
      member-ports: {{ member-ports }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
```

The following example creates a trunk to the backbone network with `ether1` and `ether2` and file server with `ether3` and `ether4`:

```yaml
mikrotik:
  interface_ethernet_switch_trunk:
    - name: backbone-trunk
      member-ports: ether1,ether2
      properties:
        comment: Conection to the backbone network
    - name: fileserver-trunk
      member-ports: ether3,ether4
```

### interface_ethernet_switch_egress-vlan-tag

Egress VLAN tagging rules on Ethernet switch interfaces are matched using their `vlan-id`. See [Manual:CRS features#Engress VLAN Tag](https://wiki.mikrotik.com/wiki/Manual:CRS_features#Egress_VLAN_Tag) for all available properties.

```yaml
mikrotik:
  interface_ethernet_switch_egress-vlan-tag:
    - vlan-id: {{ vlan-id }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
```

The following example configured egress VLAN tagging as required for the [Port Based VLAN with Trunk and Hybrid ports example](https://wiki.mikrotik.com/wiki/Manual:CRS_examples#Port_Based_VLAN):

```yaml
mikrotik:
  interface_ethernet_switch_egress-vlan-tag:
    - vlan-id: 200
      properties:
        tagged-ports: ether2,ether7,ether8
    - vlan-id: 300
      properties:
        tagged-ports: ether2,ether6,ether8
    - vlan-id: 400
      properties:
        tagged-ports: ether2,ether6,ether7
```

### interface_ethernet_switch_egress-vlan-translation / interface_ethernet_switch_ingress-vlan-translation

Egress and ingress VLAN translation rules are matched based on `ports`, `service-vlan-format`, `service-vid`, `service-pcp`, `service-dei`, `customer-vlan-format`, `customer-vid`, `customer-pcp` and the `customer-dei` which the rule matches. Only `ports` is required with the `service-vlan-format` and `customer-vlan-format` defaulting to `any` and the others not being used when matching packets. See [Manual:CRS features#Ingress/Engress VLAN Translation](https://wiki.mikrotik.com/wiki/Manual:CRS_features#Ingress.2FEgress_VLAN_Translation) for all available properties.

```yaml
mikrotik:
  interface_ethernet_switch_egress-vlan-translation:
    - ports: {{ ports }}
      service-vlan-format: {{ service-vlan-format }}
      service-vid: {{ service-vid }}
      service-pcp: {{ service-pcp }}
      service-dei: {{ service-dei }}
      customer-vlan-format: {{ customer-vlan-format }}
      customer-vid: {{ customer-vid }}
      customer-pcp: {{ customer-pcp }}
      customer-dei: {{ customer-dei }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
```

For ingress VLAN translation replace `interface_ethernet_switch_egress-vlan-translation` with `interface_ethernet_switch_ingress-vlan-translation`.

The following example translated packets tagged with a customer VLAN id of `100` to `200` on `ether[5-8]`:

```yaml
mikrotik:
  interface_ethernet_switch_egress-vlan-translation:
    - ports: ether5,ether6,ether7,ether8
      customer-vlan-format: tagged
      customer-vid: 100
      properties:
        new-customer-vid: 200
```

### interface_ethernet_switch_mac-based-vlan

MAC based VLAN rules are matched using the `src-mac-address` of the device that is assigned to a VLAN. See [Manual:CRS features#MAC Based VLAN](https://wiki.mikrotik.com/wiki/Manual:CRS_features#MAC_Based_VLAN) for all available properties.

```yaml
mikrotik:
  interface_ethernet_switch_mac-based-vlan:
    - src-mac-address: {{ src-mac-address }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
```

The following example configures MAC based VLAN as per the [MAC Based VLAN example](https://wiki.mikrotik.com/wiki/Manual:CRS_examples#MAC_Based_VLAN):

```yaml
mikrotik:
  interface_ethernet_switch_mac-based-vlan:
    - src-mac-address: A4:12:6D:77:94:43
      properties:
        new-customer-vid: 200
    - src-mac-address: 84:37:62:DF:04:20
      properties:
        new-customer-vid: 300
    - src-mac-address: E7:16:34:A1:CD:18
      properties:
        new-customer-vid: 400
```

### interface_ethernet_switch_port

Ports for switch based configuration to be applied to are matched by their `name`. See [Manual:CRS features#Port Settings](https://wiki.mikrotik.com/wiki/Manual:CRS_features#Port_Settings) for all available properties.

```yaml
mikrotik:
  interface_ethernet_switch_port:
    - name: {{ name }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
```

The following example allows MAC-based VLAN translation on `ether7`:

```yaml
mikrotik:
  interface_ethernet_switch_port:
    - name: ether7
      properties:
         allow-fdb-based-vlan-translate: yes
```

### interface_ethernet_switch_vlan

Entries in the VLAN table are matched based using their `vlan-id` and require `ports` to be specified. See [Manual:CRS features#VLAN Table](https://wiki.mikrotik.com/wiki/Manual:CRS_features#VLAN_Table) for all available properties.

```yaml
mikrotik:
  interface_ethernet_switch_vlan:
    - vlan-id: {{ vlan-id }}
      ports: {{ ports }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
```

The following example configured the VLAN table as required for the [Port Based VLAN with Trunk and Access ports example](https://wiki.mikrotik.com/wiki/Manual:CRS_examples#Port_Based_VLAN):

```yaml
mikrotik:
  interface_ethernet_switch_vlan:
    - vlan-id: 200
      ports:
        - ether2
        - ether6
      properties:
        learn: yes
    - vlan-id: 300
      ports:
        - ether2
        - ether7
      properties:
        learn: yes
    - vlan-id: 400
      ports:
        - ether2
        - ether8
      properties:
        learn: yes
```

### interface_list

Bridges are matched unsing their `name`. See [Manual:Interface/List](https://wiki.mikrotik.com/wiki/Manual:Interface/List) for all available properties.

Each list contains a list of `member` objects which are matched using by the `interface` which they add to the list.

```yaml
mikrotik:
  interface_list:
    - name: {{ name }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
      member:
        - interface: {{ interface_name }}
          properties:
            {{ property_name }}: {{ property_value }}
            ...
        ...
```

The following example creates a list of `wan` and a list of `lan` interfaces:

```yaml
mikrotik:
  interface_list:
    - name: wan
      properties:
        comment: Interfaces connecting to the internet
      member:
        - interface: ether1
          properties:
            comment: Primary WAN connection
        - interface: ether2
          properties:
            comment: Failover WAN connection
    - name: lan
      properties:
        comment: Interfaces connecting to network devices
      member:
        - interface: ether3
        - interface: ether4
        - interface: ether5
```

### ip_address

IP addresses are matched based on the `interface` and `address` (given in CIDR notation) which it defines. See [Manual:IP/Address](https://wiki.mikrotik.com/wiki/Manual:IP/Address) for all available properties.

```yaml
mikrotik:
  ip_address:
    - interface: {{ interface }}
      address: {{ address }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
    ...
```

The following example adds `10.10.1.1/24` to `ether1` and `10.10.2.1/24` to `ether2`.

```yaml
mikrotik:
  ip_address:
    - interface: ether1
      address: 10.10.1.1/24
      properties:
        comment: Subnet 1
    - interface: ether2
      address: 10.10.2.1/24
      properties:
        comment: Subnet 2
```

### ip_pool

IP address pools are matched based on `name` and requires `ranges`. See [Manual:IP/Pools](https://wiki.mikrotik.com/wiki/Manual:IP/Pools) for all available properties.

```yaml
mikrotik:
  ip_pool:
    - name: {{ name }}
      ranges: {{ ranges }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
    ...
```

The following example creates two IP address pools where the `master-pool` will use the `secondary-pool` when the `master-pool` has no free addresses.

```yaml
mikrotik:
  ip_pool:
    - name: secondary-pool
      ranges: 192.168.0.200-192.168.0.240
    - name: master-pool
      ranges: 192.168.0.10-192.168.0.99
      properties:
        next-pool: secondary-pool
```

### ip_dhcp-client

DHCP clients are matched by the `interface` which they run on. See [Manual:IP/DHCP Client](https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Client) for all available properties.

```yaml
mikrotik:
  ip_dhcp-client:
    - interface: {{ interface }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
    ...
```

The following example configures `ether1` and `ether2` with a DHCP client but ignores the peer DNS and NTP provided on `ether2`.

```yaml
mikrotik:
  ip_dhcp-client:
    - interface: ether1
    - interface: ether2
      properties:
        use-peer-dns: no
        use-peer-ntp: no
```

### ip_dhcp-server

DHCP servers are matched by the `interface` which they run on.  See [Manual:IP/DHCP Server](https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Server) for all available properties.

Static leases can be configured for each DHCP server using the `lease` key. Leases are matched by the `mac-address` and/or `client-id` which they match clients against.

```yaml
mikrotik:
  ip_dhcp-server:
    - interface: {{ interface }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
      lease:
        - mac-address: {{ mac-address }}
          client-id: {{ mac-address }}
          properties:
            {{ property_name }}: {{ property_value }}
            ...
        ...
    ...
```

**Note:** By default, new DHCP servers are disabled so adding `enabled: true` to the properties will ensure that the server is enabled.

The following example configures a DHCP server on `ether2` and `br-lan` with leases on `br-lan` being set to 7 days. The DHCP server on `br-lan` has static leases for 2 devices on the network.

```yaml
mikrotik:
  ip_dhcp-server:
    - interface: ether2
      properties:
        name: management
        address-pool: management
        enabled: 'yes'
    - interface: br-lan
      properties:
        name: lan
        address-pool: lan
        lease-time: 7d
        enabled: 'yes'
      lease:
        - mac-address: 1F:F1:25:A1:50:08
          properties:
            address: 172.16.0.2
        - client-id: 1:e9:51:33:af:b0:23
          properties:
            address: 172.16.0.3
```

### ip_dhcp-server_network

DHCP server networks are matched by the `address` range of the network. See [Manual:IP/DHCP Client#Networks](https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Server#Networks) for all available properties.

```yaml
mikrotik:
  ip_dhcp-server:
    - address: {{ address }}
      properties:
        {{ property_name }}: {{ property_value }}
        ...
    ...
```

The following example creates two networks for the DHCP server, one for the management subnet and another for the LAN:

```yaml
mikrotik:
  ip_dhcp-server_network:
    - address: 192.168.88.0/24
      properties:
        comment: management
        gateway: 192.16.88.1
    - address: 192.168.0.0/24
      properties:
        comment: lan
        gateway: 192.16.0.1
```

### system_clock

The system clock can be configured with the `system_clock` section. See [Manual:System/Time](https://wiki.mikrotik.com/wiki/Manual:System/Time#Clock_and_Time_zone_configuration) for all available properties.

```yaml
mikrotik:
  system_clock:
    {{ property_name }}: {{ property_value }}
    ...
```

The following example sets the time zone to `Europe/London`:

```yaml
mikrotik:
  system_clock:
    time-zone-name: Europe/London
```

### system_identity

The identity of the system can be configured with the `system_identity` section. See [Manual:System/identity](https://wiki.mikrotik.com/wiki/Manual:System/identity) for all available properties.

```yaml
mikrotik:
  system_dentity:
    {{ property_name }}: {{ property_value }}
    ...
```

The following example sets the system name to match the hostname used in the Ansible inventory:

```yaml
mikrotik:
  system_identity:
    name: '{{ inventory_hostname }}'
```
