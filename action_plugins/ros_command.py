# Copyright (c) 2017 Fabrizio Lungo <opensource@flungo.me>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# Support Python 3 syntax
from __future__ import absolute_import, division, print_function

from ansible import errors
from ansible.plugins.action import ActionBase

# Import the global display object or create a new one
try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

# Define static helper functions
def _format_parameter(param):
    '''
    Prepare a parameter for use in a RouterOS command.

    Converts lists, sets and booleans to string as well as quoting the output.
    '''

    # Perform type specific transforms
    if param is None:
        param = ' '
    elif isinstance(param, (list, set)):
        param = ','.join(param)
    elif isinstance(param, bool):
        param = 'yes' if param else 'no'
    elif isinstance(param, (int, float, long, complex)):
        param = str(param)
    elif not isinstance(param, (str, unicode)):
        raise errors.AnsibleActionFail('Unsupported parameter type: %s' % type(param))

    # Only quote the parameter if its not a list
    if ',' in param:
        # If the parameter needs quoting but can't be quoted because it contains a space, then raise error.
        if ' ' in param:
            raise errors.AnsibleActionFail("Parameter contains a comma and a space, cannot maintain list and spaces: %s" % param)
        # Leave lists alone
        return param
    else:
        # Quote the parameter escaping quote marks and escape marks
        return '"' + param.replace('\\', '\\\\').replace('"', '\\"') + '"'

def _create_properties_string(properties, ignore_empty=[], where=False):
    '''
    Converts a dictionary of properties into a property string.

    Returns with a space on the left.
    '''
    # Do nothing if there are no properties
    if not properties:
        return ''

    # Setup the buffer
    string = ''

    # If this is a where clause, add the where keyword
    if where:
        string+= " where"

    # Populate with properties
    for property_name, property_value in properties.items():
        # Skip properties which are empty and in ignore_empty
        if not property_value and property_name in ignore_empty:
            continue

        # Match emtpy properties using !property_name
        if where and not property_value:
            string += ' !%s' % property_name
        else:
            string += ' %s=%s' % (property_name, _format_parameter(property_value))

    return string

# must be named ActionModule or it won't be seen by Ansible
class ActionModule(ActionBase):
    TRANSFERS_FILES = False
    VALID_ARGS = frozenset(('command', 'properties', 'ignore_empty', 'put', 'where'))
    FAILURE_MSG = '#!FAILED!#'

    def run(self, tmp=None, task_vars=None):
        if task_vars is None:
            task_vars = dict()

        # Environment not supported
        if self._task.environment and any(self._task.environment):
            display.warning('ros_command module does not support the environment keyword')

        # Run boilerplate code
        result = super(ActionModule, self).run(tmp, task_vars)

        # Check for invalid arguments
        for arg in self._task.args:
            if arg not in self.VALID_ARGS:
                return {"failed": True, "msg": "'%s' is not a valid option for ros_command" % arg}

        # Get the command
        cmd = self._task.args.get('command')
        if not cmd:
            return {"failed": True, "msg": "'command' is required"}

        # Get the arguments
        properties = self._task.args.get('properties', None)
        ignore_empty = self._task.args.get('ignore_empty', [])
        put = self._task.args.get('put', False)
        where = self._task.args.get('where', False)

        # TODO: Confirm that this is handled by ActionBase
        if self._play_context.check_mode:
            # in --check mode, always skip this module execution
            result['skipped'] = True
            return result

        # Prepare the command
        cmd += _create_properties_string(properties, ignore_empty=ignore_empty, where=where)

        # if 'put' then wrap in a put command
        if put:
            cmd = ":put [%s]" % cmd

        # Wrap command to catch failures.
        # TODO: although this makes it easy to identify errors,
        #       it supresses the actual error message.
        cmd = ":do { %s } on-error={ :put %s }" % (cmd, _format_parameter(self.FAILURE_MSG))

        # Execute the command on the remote host
        # executable=False makes command raw
        # sudoable=False disables PTY
        result.update(self._low_level_execute_command(cmd, executable=False, sudoable=False))

        result['changed'] = True

        # If the return code is non-zero, fail
        if 'rc' in result and result['rc'] != 0:
            result['failed'] = True
            result['msg'] = 'non-zero return code'
        # If the last line that was output was the FAILURE_MSG, then the command failed.
        elif len(result['stdout_lines']) > 0 and result['stdout_lines'][len(result['stdout_lines']) - 1] == self.FAILURE_MSG:
            result['failed'] = True
            result['msg'] = 'command failed'

        return result
