# Copyright (c) 2017 Fabrizio Lungo <opensource@flungo.me>
# Copyright (c) 2012 Jeroen Hoekx <jeroen@hoekx.be>
# Copyright (c) 2012-2014 Michael DeHaan <michael.dehaan@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import itertools

from collections import MutableMapping, Hashable

from ansible import errors
from ansible.module_utils.six import iteritems
from ansible.module_utils.six.moves import reduce
from ansible.plugins.filter.mathstuff import unique
from ansible.utils.vars import _validate_mutable_mappings


# Based on: lib/ansible/utils/vars.py#merge_hash
def _merge_hash(a, b, union=False):
    """
    Recursively merges hash b into a so that keys from b take precedence over keys from a

    If union is True, then lists and sets within the hash that have matching keys will be unioned.
    """

    _validate_mutable_mappings(a, b)

    # if a is empty or equal to b, return b
    if a == {} or a == b:
        return b.copy()

    # if b is empty the below unfolds quickly
    result = a.copy()

    # next, iterate over b keys and values
    for k, v in iteritems(b):
        # if there's already such key in a
        # and that key contains a MutableMapping
        if k in result and isinstance(result[k], MutableMapping) and isinstance(v, MutableMapping):
            # merge those dicts recursively
            result[k] = _merge_hash(result[k], v, union=union)
        # if unioning is enabled
        # and there's already such key in a
        # and that key contains a set
        elif union and k in result and isinstance(result[k], set) and isinstance(v, set):
            # merge those sets
            result[k] = result[k] | v
        # if unioning is enabled
        # and there's already such key in a
        # and that key contains a list
        elif union and k in result and isinstance(result[k], list) and isinstance(v, list):
            # merge those lists
            result[k] = unique(result[k] + v)
        else:
            # otherwise, just copy the value from b to a
            result[k] = v

    return result

# Based on: lib/ansible/plugins/filter/core.py#combine
def smart_combine(*terms, **kwargs):
    recursive = kwargs.get('recursive', False)
    union = kwargs.get('union', False)

    for arg in kwargs:
        if arg not in ['recursive', 'union']:
            raise errors.AnsibleFilterError("'%s' is not a valid keyword argument" % arg)

    if not recursive and union:
        raise errors.AnsibleFilterError("'union' only works when 'recursive=True'")

    for t in terms:
        if not isinstance(t, dict):
            raise errors.AnsibleFilterError("|combine expects dictionaries, got " + repr(t))

    if recursive:
        return reduce((lambda a, b: _merge_hash(a, b, union=union)), terms)
    else:
        return dict(itertools.chain(*map(iteritems, terms)))


class FilterModule(object):
    ''' Combine filter with unioning '''

    def filters(self):
        filters = {
            'smart_combine': smart_combine,
        }

        return filters
